﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Piscous
{
    class Program
    {
        static void Main(string[] args)
        {
            Courant c = new Courant();
            Compte compte = (Compte) c;
            Epargne e = new Epargne();
            object oe = (object) e;
            Courant c2 = (Courant) compte;
            Console.WriteLine(compte.GetType());
            if (c2 is Courant)
            {
                Console.WriteLine(((Courant)c2).Limite);
            }
            Console.ReadLine();
        }
    }
}
