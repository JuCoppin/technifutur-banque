﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Piscous
{
    class Courant : Compte
    {
        private double _limite;

        public double Limite
        {
            get { return _limite; }
            set { _limite = value; }
        }

        public override bool retrait(double montant)
        {
            return base.retrait(montant, Limite);
        }

        protected override double calculInteret()
        {
            double total = 0;
            if (Solde > 0)
            {
                total = Solde * 3.5 / 100;
            }
            else
            {
                total = Solde * 9.75 / 100;
            }

            return total;
        }
    }
}
