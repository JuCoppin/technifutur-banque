﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Piscous
{
    class Epargne : Compte
    {
        private DateTime _dateDernierRetrait;

        public DateTime DateDernierRetrait
        {
            get { return _dateDernierRetrait; }
            set { _dateDernierRetrait = value; }
        }

        public override bool retrait(double montant)
        {
            bool retrait_ok = base.retrait(montant, 0);
            if (retrait_ok)
            {
                DateDernierRetrait = DateTime.Now;
            }

            return retrait_ok;
        }

        protected override double calculInteret()
        {
            return Solde * 4.5 / 100;
        }
    }
}
