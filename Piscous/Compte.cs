﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Piscous
{
    abstract class Compte
    {
        private string _numero;

        public string Numero
        {
            get { return _numero; }
            set { _numero = value; }
        }

        private double _solde;

        public double Solde
        {
            get { return _solde; }
            private set { _solde = value; }
        }

        private Personne _proprietaire;

        public Personne Proprietaire
        {
            get { return _proprietaire; }
            set { _proprietaire = value; }
        }

        public abstract bool retrait(double montant);

        protected abstract double calculInteret();

        protected bool retrait(double montant, double limite)
        {
            if (montant > 0 && montant <= (Solde - limite))
            {
                Solde -= montant;
                return true;
            }

            return false;
        }

        public bool depot(double montant)
        {
            if (montant > 0)
            {
                Solde += montant;
                return true;
            }

            return false;
        }

        public static double operator +(double d, Compte c)
        {
            if (c.Solde > 0)
            {
                return d + c.Solde;
            }

            return d;
        }

        public static double operator +(Compte c1, Compte c2)
        {
            double total = 0;
            if (c1.Solde > 0)
            {
                total += c1.Solde;
            }

            if (c2.Solde > 0)
            {
                total += c2.Solde;
            }

            return total;
        }

        public void appliquerInteret()
        {
            Solde += calculInteret();
        }
    }
}
