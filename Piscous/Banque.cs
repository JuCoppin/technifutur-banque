﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Piscous
{
    class Banque
    {
        private string _nom;

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        private readonly Dictionary<string, Compte> _comptes;

        private Dictionary<string, Compte> Comptes
        {
            get { return _comptes; }
        }

        public Banque(string nom)
        {
            this.Nom = nom;
            this._comptes = new Dictionary<string, Compte>();
        }

        public Compte this[string numero]
        {
            get
            {
                Compte compte;
                this._comptes.TryGetValue(numero, out compte);
                return compte;
            }
        }

        public List<Courant> getComptesCourants()
        {
            List<Courant> ls = new List<Courant>();

            foreach (Compte compte in this._comptes.Values)
            {
                if (compte is Courant)
                {
                    ls.Add((Courant)compte);
                }
            }

            return ls;
        }

        public void ajouterCompte(Compte c)
        {
            if (!this._comptes.ContainsKey(c.Numero))
            {
                this._comptes.Add(c.Numero, c);
            }
        }

        public void supprimerCompte(Compte c)
        {
            supprimerCompte(c.Numero);
        }

        public void supprimerCompte(string numero)
        {
            if (this._comptes.ContainsKey(numero))
            {
                this._comptes.Remove(numero);
            }
        }

        public double AvoirDesComptes(Personne p)
        {
            double total = 0;
            foreach (Compte c in this._comptes.Values)
            {
                if (p == c.Proprietaire)
                {
                    total += c;
                }
            }
            return total;
        }

        public void voler()
        {
            foreach (Compte compte in this._comptes.Values)
            {
                compte.retrait(5);
            }
        }

        public void Interet()
        {
            foreach (Compte compte in this._comptes.Values)
            {
                compte.appliquerInteret();
            }
        }
    }
}
