﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Piscous
{
    class Pret : Compte
    {
        public override bool retrait(double montant)
        {
            return false;
        }

        protected override double calculInteret()
        {
            return -Solde * 42 / 100;
        }
    }
}
